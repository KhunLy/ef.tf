﻿using DAL;
using DAL.Entities;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MovieContext context = new MovieContext())
            {
                //#region Selection
                //// SELECT * FROM Movies
                //Console.WriteLine("SELECT * FROM Movie");
                //IEnumerable<Movie> films = context.Movies;
                //PrintCollection(films);
                //Console.WriteLine("------------------------------------");

                //// utilisation de linq pour faire des recherches précises
                //// SELECT Movies.Title, Categories.Type
                //// FROM Movies JOIN Categories ON Categories.Id = Movies.CategoryId
                //// WHERE title LIKE 's%'  
                //Console.WriteLine("SELECT avec jointure et clause Where");
                //var films2 = context.Movies
                //    .Where(m => m.Title.StartsWith("s"))
                //    .Select(m => new { m.Title, m.Category.Type });
                //PrintCollection(films2);
                //Console.WriteLine("------------------------------------");

                //Console.WriteLine("Example DML");
                //#endregion

                //#region Ajout
                //// ajouter un film dans le Pizzeria context ne met pas à jour la db 
                ////Movie entity = context.Movies.Add(new Movie { 
                ////    Title = "Le seigneur de sanneaux",
                ////    CategoryId = 3,
                ////    Synopsis = "Un film avec un gros oeil"
                ////}).Entity;

                //// id avant saveChanges => l'id == 0
                ////Console.WriteLine(entity.Id);

                ////// l'instuction qui suit permet de synchroniser context avec la db
                ////context.SaveChanges();

                ////// id après saveChanges => id == 42
                ////Console.WriteLine(entity.Id); 
                //#endregion

                //#region Mise à jour
                ////récupérer le film que l'on veut modifier
                //// Movie aMettreAjour = context.Movies.Where(m => m.Id == 1).FirstOrDefault();
                ////Movie aMettreAjour = context.Movies.FirstOrDefault(m => m.Id == 1);
                ////Movie aMettreAjour = context.Movies.Find(1);

                ////// si le film n'a pas été recupéré depuis le context
                //EntityEntry e = context.Entry(new Movie { Id = 1, Title = "" });
                //e.State = EntityState.Modified;

                ////// mise à jour du film
                ////aMettreAjour.Synopsis = "Un autre synopsis";
                ////context.SaveChanges(); 
                //#endregion

                //#region Suppression
                ////recupération du film qu'on veut supprimer
                ////Movie toDelete = context.Movies.Find(2);

                ////// supprimer du context
                ////context.Movies.Remove(toDelete);

                ////// sauvergarder les changements
                ////context.SaveChanges(); 
                //#endregion

                //PrintCollection(context.Movies);

                MovieRepository repo = new MovieRepository(context);

                var movies = repo.GetWithLimitAndOffset(3, 3);
                    //.Select(m => new { m.Title, m.Category.Type });
                foreach (var movie in movies)
                {
                    Console.WriteLine(movie.Title);
                    Console.WriteLine(movie.Category.Type);
                }
            }
        }

        public static void PrintCollection<T>(IEnumerable<T> collection)
        {
            foreach (T item in collection)
            {
                Console.WriteLine(string.Join(" | ", typeof(T).GetProperties().Select(p => p.GetValue(item))));
            }
        }
    }
}
