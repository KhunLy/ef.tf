﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configurations
{
    class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            // changer le nom de la table
            // builder.ToTable("MyOtherTableName");

            // changer la primaryKey
            // builder.HasKey(m => m.Title);

            builder.Property(m => m.Title)
                //.HasColumnName("Autre nom pour la colonne") // renommer la colonne
                //.IsUnicode(false) // ne pas autoriser les caracteres unicode
                //.HasDefaultValue("Mon titre") // spécifier une valeur par défaut
                //.IsFixedLength() chaine de caracteres de longueur fixe
                .HasMaxLength(100) // modifier la longueur maximale
                .IsRequired(); // propriété non nullable

            builder.Property(m => m.Synopsis).IsRequired();

            // builder.HasIndex(m => m.Title).IsUnique(); ajouter un index unique sur une colonne
            // builder.HasIndex(m => new { 
            //    m.Title, m.Synopsis
            // }).IsUnique(); // ajoute un index unique sur plusieurs colonnes

            // builder.HasCheckConstraint("CK_Title", "Title LIKE a%");
            // force les titre à commencer par la lettre a

            // decrit la relation entre 2 classes d'entités
            builder.HasOne(m => m.Category)
                .WithMany(c => c.Movies)
                .HasForeignKey(m => m.CategoryId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
