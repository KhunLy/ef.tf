﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configurations
{
    class ActorConfiguration : IEntityTypeConfiguration<Actor>
    {
        public void Configure(EntityTypeBuilder<Actor> builder)
        {
            builder.Property(a => a.LastName)
                .IsUnicode(false)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(a => a.FirstName)
                .IsUnicode(false)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(a => a.BirthDate)
                .HasColumnType("DATE");

            builder.Property(a => a.Nationality)
                .IsUnicode(false)
                .HasMaxLength(2)
                .IsFixedLength();

            builder.HasIndex(a => new { a.LastName, a.FirstName }).IsUnique();
        }
    }
}
