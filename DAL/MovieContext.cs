﻿using DAL.Configurations;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MovieContext: DbContext
    {
        // le movie context contient des DBSets d'entité représentant les tables de la db
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            // définit le serveur sur lequel on veut travailler
            //optionsBuilder.UseSqlServer("server=K-PC;initial catolg=MovieDB;uid=sa;pwd=test1234=");
            optionsBuilder.UseSqlServer(@"server=K-PC;initial catalog=TFMovieDB;integrated security=true");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            // appliquer une configuration sur une table
            modelBuilder.ApplyConfiguration(new MovieConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ActorConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
        }
    }
}
