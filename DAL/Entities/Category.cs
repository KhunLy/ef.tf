﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public ICollection<Movie> Movies { get; set; } 
        // navigation Properties décrivant la relation entre les entités
        // ne pas oublier la foreign key et la réciproque
    }
}
