﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Role
    {
        public int ActorId { get; set; } // Foreign Key
        public Actor Actor { get; set; }

        public int MovieId { get; set; } // foereign key
        public Movie Movie { get; set; }
        public string RoleName { get; set; }
    }
}
