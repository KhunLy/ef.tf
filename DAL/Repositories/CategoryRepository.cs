﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CategoryRepository : RepositoryBase<Category, int>
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }
    }
}
