﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    // Si la clé primaire de la table est une clé composite
    // le type de la clé sera un tableau d'objet
    public class RoleRepository : RepositoryBase<Role, object[]>
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }
    }
}
