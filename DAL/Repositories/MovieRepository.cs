﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class MovieRepository : RepositoryBase<Movie,int>
    {
        public MovieRepository(MovieContext context) : base(context)
        {
            
        }

        // recuperer tous les films d'une certaine categorie
        public IEnumerable<Movie> GetByCategory(int categoryId) 
        {
            return _context.Set<Movie>()
                .Where(m => m.CategoryId == categoryId);
        }

        // recuperer tous les films dont le titre contient le mot clé
        public IEnumerable<Movie> GetByTitle(string keyword) 
        {
            // SELECT * FROM Movies WHERE Tiltle LIKE '%keyword%'
            return _context.Set<Movie>()
                .Where(m => m.Title.Contains(keyword));
        }

        // récupérer une cetaine limite de films à partir d'un certain rang
        // triés par ordre croissant sur la categorie puis sur le titre (skip, take)
        public IEnumerable<Movie> GetWithLimitAndOffset(int limit, int offset) 
        { 
            return _context.Set<Movie>()
                //.Include(m => m.Category) // forcer la jointure
                //.OrderBy(m => m.CategoryId)
                //.ThenBy(m => m.Title)
                .OrderBy(m => m.Title)
                .Skip(offset)
                .Take(limit)
                ;
        }
    }
}
