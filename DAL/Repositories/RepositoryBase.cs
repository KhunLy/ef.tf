﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public abstract class RepositoryBase<TEntity, TKey>
        where TEntity : class
    {
        protected readonly DbContext _context;

        protected RepositoryBase(DbContext context)
        {
            _context = context;
        }

        public IQueryable<TEntity> GetAll() 
        {
            return _context.Set<TEntity>();
        }

        public TEntity GetById(TKey key) 
        {
            return _context.Find<TEntity>(key);
        }

        public TEntity Insert(TEntity entity) 
        {
            TEntity saved = _context.Add(entity).Entity;
            _context.SaveChanges();
            return saved;
        }

        public bool Update(TEntity entity) 
        { 
            EntityEntry entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
            return _context.SaveChanges() > 0;
        }

        public bool Delete(TKey key) 
        {
            TEntity entity = _context.Find<TEntity>(key);
            _context.Remove(entity);
            return _context.SaveChanges() > 0;
        }
    }
}
